
#                       TD 7


#     Exercice 1





# Changement de r�pertoire

setwd(dir="C:/Users/liljo/OneDrive/Bureau/TD7D_A2020")        #C:

#  Importation des donn�es de Coton.csv

Coton <- read.csv("Coton.csv", header = TRUE, sep = ";",dec = ",",col.names=c('PERCENT'))

Coton   # Permet d'afficher les donn�es stock�es dans Coton.csv. Coton est elle-m�me une
        # base de donn�es

X<- Coton$PERCENT # Permet de choisir un nom de variable plus court.
X
#======================================================================================

# Analyse statistique compl�te de la variable "PERCENT"

#======================================================================================


#                      Histogramme

hist(Coton$PERCENT)        # Cas simple 

# Ou bien en utilisant le nom de la variable, c'est-�-dire X
hist(X)

#--------------------------------------------------------------------------------------

hist(Coton$PERCENT, col="lightpink",border="black", main=paste("HISTOGRAMME"),
     xlab="Pourcentages de coton",ylab="Fr�quences")

# Ou bien en utilisant le nom de la variable, c'est-�-dire X
hist(X, col="lightpink",border="black", main=paste("HISTOGRAMME"),
     xlab="Pourcentages de coton",ylab="Fr�quences")
#======================================================================================


#                     Tableau d'effectifs


TabEff=table(Coton$PERCENT)                # ou bien TabEff=table(X)
TabEff                    # Affiche le tableau



# Pour obtenir un tableau des fr�quences, il faut cr�er des limites de classes

breaks  <-seq(32,38,by=1)  #  by donne la longueur de l'intervalle (essayer diff�rentes  valeurs)
breaks

classx  <-  factor(cut(Coton$PERCENT,  breaks))  # ou bien, remplacer Coton$PERCENT par X

xout  <-  as.data.frame(table(classx))
xout       # Tableau des fr�quences

# On peut ajouter des effectifs cumul�s et des fr�quences relatives: 
# Voir page 4 du pdf TD7-MTH2302D

#======================================================================================

#        Diagramme de boite � moustaches (Boxplot)


boxplot(Coton$PERCENT) # Cas le plus simple

boxplot(Coton$PERCENT,
        col=c("lightblue"),
        horizontal=TRUE,
        notch=TRUE,                    # Permet de d�ciner des boites en forme de 8
        main=paste("Pourcentages de coton"),
        ylab="Lot de tissus No1",
        las=1)

#======================================================================================

#        graphique de probabilit� normale et graphique quantile-quantile


qqnorm(Coton$PERCENT)                       # Sur �chelle de proba normale
qqline(Coton$PERCENT, col="red", lwd=2)     # Quantile-quantile



#=====================================================================================

#    Moyenne, variance, �cart-type, coefficient de variation, m�diane, q1, q3, eiq


m=mean(X)  # ou bien m=mean(Coton$PERCENT): Moyenne de X ou de Coton$PERCENT

md=median(X) # M�diane de X

v=var(X)  # Variance de X

s=sd(X)   # Ecart-type de X, (sd= "standard deviation" en anglais)

cv=s/m    # Coefficient de variation

# cat() permet d'afficher et est utiliser de la fa�on suivante:
cat('moyenne =',m,', m�diane =',md,', �cart-type =',s,', variance =',v,'\n',
    'coefficient de variation =',cv)

# La commande 'summary()' donne certaines statistiques : le minimum, le 1er quartile,
# la m�diane, la moyenne, le 3eme quartile et le maximum.

summary(X)

# Les percentiles de tout ordre peuvent �tre calcul�s avec la commande quantile(X, ordres)

quantile(X, 0.5)  # la m�diane

quantile(X, c(0.25,0.5,0.75))  # pour les 3 quartiles

# L'�cart inter quartile (commande IQR)

IQR(X)


                 #Autre fa�on: pr�senter les r�sultats sous forme d'un tableau


#     Cr�ation d'un tableau de calculs

# Initialisation du data.frame

mesures = data.frame(param�tres=c("Valeurs"),moyenne=NA, var=NA, s=NA, q1=NA, mediane=NA,
                     q3=NA, cv=NA, eiq=NA)

mesures

#moyenne
mesures$moyenne = sapply(1, function(i) mean(Coton[,i]))

# �cart-type
mesures$s = sapply(1, function(i) sd(Coton[,i]))

# Variance
mesures$var=(mesures$s)^2

# Coefficient de variation
mesures$cv=mesures$s / mesures$moyenne

# mediane
mesures$mediane = sapply(1, function(i) median(Coton[,i]))

# quantiles q1 et q3
mesures[1, c("q1", "q3")] = quantile(Coton$PERCENT, probs = c(0.25,0.75))

# Ecart interquartile
mesures$eiq = mesures$q3 - mesures$q1




















